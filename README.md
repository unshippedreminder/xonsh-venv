A personal Python Virtual Environment containing xonsh and the xontribs i use.

# Install

```bash
# Install dependencies
sudo rpm-ostree install -A python3-prompt-toolkit python3-pygments python3-setproctitle
# Install the VENV
sudo git clone https://gitlab.com/unshippedreminder/xonsh-venv.git /var/opt/xonsh-venv

# Install and enable maintenance service
cd /var/opt/xonsh-venv/bin
sudo ./upgrade-xonsh-venv.sh

# Add Xonsh to /etc/shells
echo "/var/opt/xonsh-venv/bin/xonsh" >> /etc/shells

# Set shell to Xonsh
sudo usermod --shell /var/opt/xonsh-venv/bin/xonsh $USER
```
