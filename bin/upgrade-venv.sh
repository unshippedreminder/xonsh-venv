#!/usr/bin/bash

cd /var/opt/xonsh-venv/bin

# Must be run as root
if [ $(whoami) == 'root' ]; then
  echo "Upgrading Pip."
  ./pip install --upgrade pip
  echo "Upgrading Xonsh dependencies."
  ./python -m pip install -r ../requirements.txt --upgrade-strategy eager
  # Skip everything related to systemd if running in a container.
  if [ ! -v container ]; then

    xonsh_service_installed_path='/etc/systemd/system/xonsh-venv.service'
    xonsh_service_non_installed_path='../xonsh-venv.service'

    xonsh_service_installed_content=$(cat $xonsh_service_installed_path)
    xonsh_service_non_installed_content=$(cat $xonsh_service_non_installed_path)

    echo "Has xonsh-venv.service changed?"
    if [ xonsh_service_installed_content != xonsh_service_non_installed_content ]; then
      echo "xonsh-venv.service has changed. Updating file..."
      cp ../xonsh-venv.service /etc/systemd/system/xonsh-venv.service
      echo "Reloading systemctl daemon"
      systemctl daemon-reload
    else
      echo "xonsh-venv.service has not changed."
    fi
    # If xonsh-venv.service is not enabled, enable and run it now.
    echo "Is xonsh-venv.service enabled?"
    if [ $(systemctl is-enabled xonsh-venv.service) != 'enabled' ]; then
      echo "xonsh-venv.service is not enabled. Enabling now..."
      systemctl enable --now xonsh-venv.service
    else
      echo "xonsh-venv.service is already enabled."
    fi
  else
    echo "Running in a container. Skipping systemd service setup..."
  fi
else
  echo "Must be run as root"
  exit 1
fi
